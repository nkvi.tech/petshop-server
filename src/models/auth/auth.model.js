import mongoose from 'mongoose'
const { Schema } = mongoose


const schema = new Schema(
    {
        username: { type: Schema.Types.String },
        password: { type: Schema.Types.String },
        email: { type: Schema.Types.String },
        firstName: { type: Schema.Types.String },
        lastName: { type: Schema.Types.String },
        gender: { type: Schema.Types.String },
    },
    { timestamps: true });

export default mongoose.model('user', schema);
