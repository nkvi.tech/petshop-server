import mongoose from 'mongoose'
const { Schema } = mongoose


const schema = new Schema(
    {
        adaptability: { type: Schema.Types.Number },
        affection_level: { type: Schema.Types.Number },
        alt_names: { type: Schema.Types.String },
        cfa_url: { type: Schema.Types.String },
        child_friendly: { type: Schema.Types.Number },
        country_code: { type: Schema.Types.String },
        country_codes: { type: Schema.Types.String },
        description: { type: Schema.Types.String },
        dog_friendly: { type: Schema.Types.Number },
        energy_level: { type: Schema.Types.Number },
        experimental: { type: Schema.Types.Number },
        grooming: { type: Schema.Types.Number },
        hairless: { type: Schema.Types.Number },
        health_issues: { type: Schema.Types.Number },
        hypoallergenic: { type: Schema.Types.Number },
        id: { type: Schema.Types.String },
        image: {
            height: { type: Schema.Types.Number },
            id: { type: Schema.Types.String },
            url: { type: Schema.Types.String },
            width: { type: Schema.Types.Number }
        },
        indoor: { type: Schema.Types.Number },
        intelligence: { type: Schema.Types.Number },
        lap: { type: Schema.Types.Number },
        life_span: { type: Schema.Types.String },
        name: { type: Schema.Types.String },
        natural: { type: Schema.Types.Number },
        origin: { type: Schema.Types.String },
        rare: { type: Schema.Types.Number },
        reference_image_id: { type: Schema.Types.String },
        rex: { type: Schema.Types.Number },
        shedding_level: { type: Schema.Types.Number },
        short_legs: { type: Schema.Types.Number },
        social_needs: { type: Schema.Types.Number },
        stranger_friendly: { type: Schema.Types.Number },
        suppressed_tail: { type: Schema.Types.Number },
        specie: { type: Schema.Types.String },
        temperament: { type: Schema.Types.String },
        vcahospitals_url: { type: Schema.Types.String },
        vetstreet_url: { type: Schema.Types.String },
        vocalisation: { type: Schema.Types.Number },
        weight: {
            imperial: { type: Schema.Types.String },
            metric: { type: Schema.Types.String }
        },
        wikipedia_url: { type: Schema.Types.String }
    },
    { timestamps: true });

export default mongoose.model('pet', schema);
