import AuthModel from '../../models/auth/auth.model.js'
import * as  AuthService from "../../services/auth/auth.service.js"


export const authJwt = async (req, res, next) => {
    try {
        const token = AuthService.getTokenFromHeaders(req)
        console.log(token);
        if (!token) throw new Error()

        const user = await AuthModel.findById(token._id)
        if (!user) throw new Error()

        req.user = user
        next()
    } catch (error) {
        console.log(error)
        req.user = null
        res.sendStatus(402)
    }
}

export const check = async (req, res) => {
    try {
        const jwtToken = AuthService.createToken(req.user)
        return res.status(200).json({ data: { token: jwtToken, user: req.user } })
    } catch (error) {
        res.status(400).json(error)
    }
}
export const logIn = async (req, res) => {
    try {
        const user = await AuthModel.findOne(req.body);
        if (!user) throw new Error()
        const jwtToken = AuthService.createToken(user)
        res.status(200).json({ token: jwtToken, user: user })
    } catch (error) {
        res.status(500).json({ message: "The username or password provided is incorrect." })
    }
}
export const signUp = async (req, res) => {
    try {
        // const { username } = req.body
        // const userAuth = await AuthModel.findOne({ username: username })
        // If user  exists
        // if (userAuth) {
        //     res.status(400).json({ message: 'Username is not available' })
        // }
        // If user dose not exists
        const newUser = await AuthModel.create(req.body);
        const user = await AuthModel.findOne(newUser);
        const jwtToken = AuthService.createToken(user)
        return res.status(200).json({ token: jwtToken, user: user })
    } catch (error) {
        return res.status(401).json({ message: "Error sign up" })
    }
}

