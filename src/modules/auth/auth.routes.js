import express from 'express';
import * as controllers from './auth.controllers.js'

const authRoutes = new express.Router()

authRoutes.get('/check', controllers.authJwt, controllers.check)
authRoutes.post('/signup', controllers.signUp)
authRoutes.post('/login', controllers.logIn)

export default authRoutes