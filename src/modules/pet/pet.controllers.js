import PetModel from '../../models/pet/pet.model.js'

export const getPets = async (req, res) => {
    try {
        const { page = 1, perPage = 10 } = req.query;
        const data = await PetModel.find().skip(parseInt((page - 1) * perPage)).limit(parseInt(perPage, 10));
        const total = await PetModel.count();
        res.status(200).json({ data, total });
    } catch (error) {
        console.log(error)
        res.status(500).json({ message: error.message })
    }

}

export const getPetById = async (req, res) => {
    try {
        const { id } = req.params;
        const data = await PetModel.findOne({ _id: id });
        res.status(200).json({ data });
    } catch (error) {
        console.log(error)
        res.status(500).json({ message: error.message })
    }

}
export const createPet = async (req, res) => {
    try {
        console.log(req.body);
        await PetModel.create(req.body);
        res.status(200).json({ message: 'successfully' });
    } catch (error) {
        console.log(error)
        res.status(500).json({ message: error.message })
    }

}

export const updatePetById = async (req, res) => {
    try {
        const data = await PetModel.findOneAndUpdate({ _id: req.body._id }, req.body);
        res.status(200).json({ data });
    } catch (err) {
        console.log(error)
        res.status(500).json({ message: error.message })
    }

}

export const deletePetById = async (req, res) => {
    try {
        const { id } = req.params;
        await PetModel.findByIdAndDelete(id);
        res.status(200).json({ message: 'successfully' });
    } catch (error) {
        console.log(error)
        res.status(500).json({ message: error.message })
    }

}



