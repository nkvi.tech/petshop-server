import express from 'express';
const petRoutes = express.Router();
import * as controllers from './pet.controllers.js';

petRoutes.get('/', controllers.getPets);
petRoutes.get('/:id', controllers.getPetById);
petRoutes.post('/create', controllers.createPet);
petRoutes.put('/update/:id', controllers.updatePetById);
petRoutes.delete('/delete/:id', controllers.deletePetById);

export default petRoutes;