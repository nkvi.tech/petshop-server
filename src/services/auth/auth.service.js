import jwt from "jsonwebtoken"

export const createToken = user => {
    if (!user && !user.id) {
        return null
    }

    const payload = { id: user.id }
    return jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: process.env.JWT_OPTS_EXPIRE_IN, issuer: process.env.JWT_OPTS_ISSUER })
}

export const verifyToken = token => {
    try {
        return jwt.verify(token, process.env.JWT_SECRET, { expiresIn: process.env.JWT_OPTS_EXPIRE_IN, issuer: process.env.JWT_OPTS_ISSUER })
    } catch (e) {
        return jwt.verify(token, process.env.JWT_SECRET, { expiresIn: '7d', issuer: 'CLIENT' })
    }
}

export const getTokenFromHeaders = req => {
    const token = req.headers.authorization

    if (token) {
        const arr = token.split(" ")

        if (arr[0] === "Bearer" && arr[1]) {
            try {
                return verifyToken(arr[1])
            } catch (error) {
                return null
            }
        }
    }

    return null
}


