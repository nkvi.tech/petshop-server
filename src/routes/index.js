import petRoutes from '../modules/pet/pet.routes.js'
import authRoutes from '../modules/auth/auth.routes.js'

const useRoutes = (app) => {
    app.use('/v1/pets', petRoutes);
    app.use('/v1/auth', authRoutes);
}


export default useRoutes;